package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
 public String evaluate(String statement) {
        if (statement == null || statement.equals("")) return null;
        LinkedList<String> withParentheses = new LinkedList<>();
        for (int i = 0; i < findNumbers(statement).length; i++){
            if (findNumbers(statement)[i] != null){
                withParentheses.add(findNumbers(statement)[i]);
            }
        }
        while (withParentheses.contains("(") || withParentheses.contains(")")){
            String [] arr = new String[withParentheses.size()];
            withParentheses.toArray(arr);
            withParentheses = removeParentheses(arr);
        }
        String result = calculate(withParentheses);
        if (result != null){
            double d = Double.parseDouble(result);
            int i = (int) d;
            if((d - i) < 0.000001)
                return String.valueOf(i);
            else {
                return String.valueOf((double) Math.round(d*10000)/10000);
            }
        } else return null;
    }


    private String [] findNumbers (String s){
        char [] charArray = s.toCharArray ();
        String [] numsAndDelimiters = new String[s.length()];
        int k = 1;
        numsAndDelimiters [0] = String.valueOf(charArray [0]);
        for (int i = 1; i < charArray.length; i++) {
            if ((Character.isDigit(charArray[i])||(charArray[i] == '.')) && (Character.isDigit(charArray[i-1])||(charArray[i-1] == '.'))) {
                numsAndDelimiters[k-1] = numsAndDelimiters[k-1] + String.valueOf(charArray[i]);
            } else {
                numsAndDelimiters [k] = String.valueOf(charArray[i]);
                k++;
            }
        }
        return numsAndDelimiters;
    }

    private LinkedList <String> removeParentheses (String [] s){
        LinkedList <String> inPar = new LinkedList<>();
        LinkedList <String> allStr = new LinkedList<>();
        int left = 0;
        int right = 0;
        int leftcount = 0;
        int rightcount = 0;
        for (int i = 0; i < s.length; i++) {
            if ("(".equals(s[i])) {
                left = i;
                leftcount++;
            }
        }
        for (int i = left; i < s.length; i++) {
            if (")".equals(s[i])) {
                right = i;
                break;
            }
        }
        if ((left > right) || ((leftcount > 0) && (right == 0))) return allStr;

        inPar.addAll(Arrays.asList(s).subList(left + 1, right));
        allStr.addAll(Arrays.asList(s).subList(0, left));
        allStr.add(calculate(inPar));
        for (int i = right + 1; i < s.length; i++){
            if(s[i] != null)
                allStr.add(s[i]);
        }
        return allStr;
    }

    private String calculate (LinkedList <String> noParenthese) {
        try {
            for (int i = 0; i < noParenthese.size(); i++) {
                if (noParenthese.get(i).equals("*")) {
                    noParenthese.set(i + 1, String.valueOf(Double.parseDouble(noParenthese.get(i - 1)) * Double.parseDouble(noParenthese.get(i + 1))));
                    noParenthese.remove(i-1);
                    noParenthese.remove(i-1);
                    i--;
                }
                if (noParenthese.get(i).equals("/")) {
                    noParenthese.set(i + 1, String.valueOf(Double.parseDouble(noParenthese.get(i - 1)) / Double.parseDouble(noParenthese.get(i + 1))));
                    noParenthese.remove(i-1);
                    noParenthese.remove(i-1);
                    i--;
                }
            }

            for (int i = 0; i < noParenthese.size(); i++) {
                if (noParenthese.get(i).equals("+")) {
                    noParenthese.set(i + 1, String.valueOf(Double.parseDouble(noParenthese.get(i - 1)) + Double.parseDouble(noParenthese.get(i + 1))));
                    noParenthese.remove(i-1);
                    noParenthese.remove(i-1);
                    i--;
                }
                if (noParenthese.get(i).equals("-")) {
                    noParenthese.set(i + 1, String.valueOf(Double.parseDouble(noParenthese.get(i - 1)) - Double.parseDouble(noParenthese.get(i + 1))));
                    noParenthese.remove(i-1);
                    noParenthese.remove(i-1);
                    i--;
                }
            }
            if ((noParenthese.size() == 1) && (Double.parseDouble(noParenthese.get(0)) < Integer.MAX_VALUE)){
                return noParenthese.get(0);
            }
            else return null;
        } catch (Exception e){
            return null;
        }
    }
}
