package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int lineCount = 1;
        int numsInLine = 1;
        try {
            int [] input = new int[inputNumbers.size()];
        for (int i = 0; i < inputNumbers.size(); i++){
            input[i] = inputNumbers.get(i);
        }
        Arrays.sort(input);
        int lCounter = 0;
            for (int i = 1; i <= input.length; i++){
                lCounter +=i;
                if (lCounter == input.length) break;
                lineCount++;
                numsInLine += 2;
            }
        int counter = 0;
        int lenghtCounter = 1;
        int[][] result = new int[lineCount][numsInLine];
        for (int i = 1; i <= lineCount; i++){
            int [] withzero = new int[numsInLine];
            System.arraycopy(input,counter,withzero,withzero.length/2-i+1,lenghtCounter);
            counter += i;
            lenghtCounter++;
            withzero = addZero(withzero);
            for (int j = 0; j < numsInLine; j++){
                result [i - 1] [j] = withzero[j];
            }
        }
            return result;
        } catch (Throwable e){
            throw new CannotBuildPyramidException ();
        }
    }

    private int [] addZero(int[] nums) {
        int [] newWithzero = new int[nums.length];
        int cnt = 0;
        for (int i = 0; i <= nums.length/2; i++){
            if (nums [i] == 0) {
                newWithzero [cnt] = nums [i];
                cnt++;
            } else {
                newWithzero [cnt] = nums [i];
                cnt = cnt + 2;
            }
        }
        return newWithzero;
    }
}
